package st.rhapsody.voxelengine.test.terrain.block;

import st.rhapsody.voxelengine.lib.terrain.block.Block;

/**
 * Created by nicklaslof on 21/01/15.
 */
public class Air extends Block {
    protected Air(byte id) {
        super(id, "","","");
    }
}
