package st.rhapsody.voxelengine.lib.terrain.block;

/**
 * Created by nicklas on 5/8/14.
 */
public interface IBlockProvider {

    public Block getBlockById(byte blockId);
}
